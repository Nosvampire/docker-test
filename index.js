'use-strict';
const express = require('express');
const app = express();

const { routes } = require('./src/routes/routes');

const { getLogger } = require('./src/infrastructure/logger.util');

async function serverStart() {
    const logger = getLogger({ name: 'serverStart' });

    const port = process.env.PORT || 3000;

    app.use(express.json());

    routes(app);

    app.listen(port, () => {
        // console.log(`App is listening... on port: ${port}`);
        logger.info(
            JSON.stringify({
                name: 'listen',
                msg: `App is listening... on port: ${port}`
            })
        );
    });
}

serverStart();
