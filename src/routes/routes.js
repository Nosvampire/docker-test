"use strict";
const { getLogger } = require('../infrastructure/logger.util');

function routes(app) {
    const logger = getLogger({ name: 'routes' });

    app.get('/healthcheck', (req, res) => {
        logger.info(JSON.stringify({ name: 'healthcheck' }));

        const response = {
            code: 200,
            message: 'OK',
            payload: 'OK'
        };

        res.status(200).json(response);
    });
}

module.exports = { routes };