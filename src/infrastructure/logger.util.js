const pino = require('pino');

/**
 * Obtiene un logger configurado
 * @param customArgs
 * @returns {*}
 */
function getLogger(customArgs) {
    const logger = getStaticLogger();

    if (!customArgs) {
        return logger;
    }

    if (typeof customArgs !== 'object') {
        throw new Error('getLogger: customArgs debe ser de tipo object');
    }

    return logger.child(customArgs);
}

function getStaticLogger(args) {
    // let loggerLevel = 'info';
    // if (process.env.LOGGER_LEVEL) {
    //     let level = process.env.LOGGER_LEVEL;
    //     if (level.match(/^(fatal|error|warn|info|debug|trace|silent)$/)) {
    //         loggerLevel = level;
    //     }
    // }

    const LOGGER_LEVEL = process.env.LOGGER_LEVEL;

    const loggerLevel = /^(fatal|error|warn|info|debug|trace|silent)$/.test(LOGGER_LEVEL) ? LOGGER_LEVEL : 'info';

    const staticLogger = pino({
        base: {},
        level: loggerLevel,
        ...args
    });

    return staticLogger;
}

module.exports = {
    getLogger,
    getStaticLogger
};